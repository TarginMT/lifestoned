﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lifestoned.DataModel.Gdle.Recipes;

namespace Lifestoned.Providers
{
    public interface IRecipeProvider : IGenericContentProvider<Recipe>
    {
    }

    public interface IRecipeSandboxProvider : IRecipeProvider, IGenericSandboxContentProvider<Recipe>
    {
    }
}
