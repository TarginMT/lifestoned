﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using log4net;
using Newtonsoft.Json;

using Lifestoned.DataModel.Shared;

namespace Lifestoned.Providers.Database
{
    public class SQLiteDatabaseProvider : DatabaseProviderBase
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected SQLiteDatabaseProvider(string connectionName) : base(connectionName)
        {
        }

        protected override void OnSetupConnection(DbConnection connection)
        {
            SQLiteConnection db = connection as SQLiteConnection;
            if (db != null)
            {
                try
                {
                    db.EnableExtensions(true);
                    try
                    {
                        var res = db.Query("select * from json_each(json('[0]'))");
                    }
                    catch
                    {
                        db.LoadExtension("SQLite.Interop.dll", "sqlite3_json_init");
                    }
                }
                catch
                {
                }
            }
        }
    }

    public class SQLiteContentDatabase<T> : SQLiteDatabaseProvider, IGenericContentProvider<T> where T : class, IMetadata
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected string TableName { get; set; }

        protected Func<T, uint> GetId { get; set; }

        public SQLiteContentDatabase(string connectionName, string tableName, Func<T, uint> idfn) : base(connectionName)
        {
            TableName = tableName;
            GetId = idfn;
        }

        protected override void OnInitialize(DbConnection connection)
        {
            connection.Execute($@"
				CREATE TABLE IF NOT EXISTS {TableName} (
				id INT PRIMARY KEY,
				lastModified DATETIME,
				userModified VARCHAR(100) COLLATE NOCASE,
				jsonData TEXT COLLATE NOCASE);");
        }

        #region IGenericContentProvider

        public IQueryable GetContent()
        {
            return Get();
        }

        public object GetContent(uint id)
        {
            return Get(id);
        }

        public bool SaveContent(object item)
        {
            return Save(item as T);
        }

        public bool UpdateContent(object item)
        {
            return Update(item as T);
        }

        public bool DeleteContent(uint id)
        {
            return Delete(id);
        }

        public virtual bool Delete(uint id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                    db.Execute($"DELETE FROM {TableName} WHERE id=@id", new { id });
                return true;
            }
            catch (DbException)
            {
                return false;
            }
        }

        public virtual IQueryable<T> Get()
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    var query = db.Query<string>($"SELECT jsonData FROM {TableName}");
                    return query.Select(content => JsonConvert.DeserializeObject<T>(content)).AsQueryable();
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public virtual T Get(uint id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    string content = db.ExecuteScalar<string>($"SELECT jsonData FROM {TableName} WHERE id=@id", new { id });
                    return JsonConvert.DeserializeObject<T>(content);
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public virtual bool Save(T item)
        {
            return Update(item);
        }

        public virtual bool Update(T item)
        {
            try
            {
                string content = JsonConvert.SerializeObject(item);

                using (DbConnection db = GetConnection())
                {
                    db.Execute($@"
						INSERT INTO {TableName} (id, lastModified, userModified, jsonData)
						VALUES (@id, @LastModified, @ModifiedBy, @content)
						ON CONFLICT (id) DO UPDATE SET
						lastModified = @LastModified, userModified = @ModifiedBy, jsonData = @content
						WHERE id = @id;
						", new
                    {
                        id = GetId(item),
                        item.LastModified,
                        item.ModifiedBy,
                        content
                    });
                    return true;
                }
            }
            catch (DbException ex)
            {
                return false;
            }
        }

        public virtual IQueryable<T> Search(Expression<Func<T, bool>> criteria)
        {
            return Search(criteria, 0);
        }

        public virtual IQueryable<T> Search(Expression<Func<T, bool>> criteria, int limit)
        {
            if (criteria == null)
                return Enumerable.Empty<T>().AsQueryable();

            Expression node = criteria.Body;

            if (node.CanReduce)
                node = node.Reduce();

            DynamicParameters queryParams = new DynamicParameters();

            StringBuilder query = new StringBuilder($"SELECT jsonData FROM {TableName} WHERE ");

            AppendNode(query, criteria, criteria.Parameters, queryParams);

            if (limit > 0)
                query.Append($" LIMIT {limit}");

            ////log.Debug($"Attempting query: {query.ToString()}");
            ////foreach (var p in queryParams.ParameterNames)
            ////    log.Debug($"    {p} => {queryParams.Get<object>(p)} ({queryParams.Get<object>(p).GetType()})");

            try
            {
                using (DbConnection db = GetConnection())
                {
                    var results = db.Query<string>(query.ToString(), queryParams);
                    return results.Select(content => JsonConvert.DeserializeObject<T>(content)).AsQueryable();
                }
            }
            catch (DbException)
            {
                return Enumerable.Empty<T>().AsQueryable();
            }
        }

        #endregion

        #region Search/Query Builder

        protected void AppendNode(StringBuilder builder, Expression node, ReadOnlyCollection<ParameterExpression> parameters, DynamicParameters queryParams, string json = "jsonData")
        {
            string paramName = $"@p{queryParams.ParameterNames?.Count() ?? 0}";
            switch (node.NodeType)
            {
                case ExpressionType.AndAlso:
                    AppendBinaryNodes(builder, node, "AND", parameters, queryParams, json);
                    break;
                case ExpressionType.OrElse:
                    AppendBinaryNodes(builder, node, "OR", parameters, queryParams, json);
                    break;

                case ExpressionType.NotEqual:
                    BinaryExpression b = node as BinaryExpression;
                    AppendBinaryNodes(builder, node, b.IsLifted ? "IS NOT" : "<>", parameters, queryParams, json);
                    break;
                case ExpressionType.Equal:
                    AppendEqualLikeNodes(builder, node, parameters, queryParams, json);
                    break;

                case ExpressionType.GreaterThan:
                    AppendBinaryNodes(builder, node, ">", parameters, queryParams, json);
                    break;
                case ExpressionType.GreaterThanOrEqual:
                    AppendBinaryNodes(builder, node, ">=", parameters, queryParams, json);
                    break;
                case ExpressionType.LessThan:
                    AppendBinaryNodes(builder, node, "<", parameters, queryParams, json);
                    break;
                case ExpressionType.LessThanOrEqual:
                    AppendBinaryNodes(builder, node, "<=", parameters, queryParams, json);
                    break;

                case ExpressionType.Constant:
                    ConstantExpression c = (ConstantExpression)node;
                    ////log.Info($"Constant Node {c.Value} {c}");
                    ////if (c.Value is DateTime)
                    ////    builder.Append($"'{c.Value}'");
                    ////else if (c.Value == null)
                    ////    builder.Append("NULL");
                    ////else
                    ////    builder.Append(c.Value.ToString());
                    if (c.Value == null)
                        builder.Append("NULL");
					else
                    {
                        queryParams.Add(paramName, c.Value);
                        builder.Append(paramName);
                    }
                    break;

                case ExpressionType.MemberAccess:
                    AppendMember(builder, node, parameters, queryParams, json);
                    break;

                case ExpressionType.Convert:
                    UnaryExpression convert = (UnaryExpression)node;
                    AppendNode(builder, convert.Operand, parameters, queryParams, json);
                    break;

                case ExpressionType.IsTrue:
                    builder.Append("true");
                    break;
                case ExpressionType.IsFalse:
                    builder.Append("false");
                    break;

                case ExpressionType.Lambda:
                    LambdaExpression l = (LambdaExpression)node;
                    AppendNode(builder, l.Body, parameters, queryParams, json);
                    break;

                case ExpressionType.Conditional:
                    ConditionalExpression ifc = (ConditionalExpression)node;
                    if (EvaluateTest(ifc.Test))
                        AppendNode(builder, ifc.IfTrue, parameters, queryParams, json);
                    else
                        AppendNode(builder, ifc.IfFalse, parameters, queryParams, json);
                    break;

                case ExpressionType.Call:
                    MethodCallExpression call = (MethodCallExpression)node;
                    if (call.Method.DeclaringType == typeof(Enumerable))
                    {
                        if (call.Method.Name == "Any")
                            AppendExistsInListNode(builder, call, parameters, queryParams, json);
                    }
                    else
                    {
                        ////log.Info($"Constant Node {call}");
                        object val = Expression.Lambda(node).Compile().DynamicInvoke();
                        queryParams.Add(paramName, val);
                        builder.Append(paramName);
                        ////if (val is DateTime)
                        ////    builder.Append($"'{val:o}'");
                        ////else
                        ////    builder.Append(val.ToString());
                    }
                    break;

                case ExpressionType.ExclusiveOr:
                case ExpressionType.MemberInit:
                case ExpressionType.Modulo:
                case ExpressionType.Negate:
                case ExpressionType.NegateChecked:
                case ExpressionType.Not:
                case ExpressionType.Parameter:
                case ExpressionType.TypeEqual:

                default:
                    log.Warn($"Unhandled Expression Type {node.NodeType}");
                    break;
            }
        }

        protected void AppendBinaryNodes(StringBuilder builder, Expression node, string op, ReadOnlyCollection<ParameterExpression> parameters, DynamicParameters queryParams, string json)
        {
            BinaryExpression e = node as BinaryExpression;
            if (e == null)
            {
                ////Logger.LogMessage($"Not a Binary Expression Type {node.NodeType}");
                return;
            }

            StringBuilder left = new StringBuilder();
            StringBuilder right = new StringBuilder();

            AppendNode(left, e.Left, parameters, queryParams, json);
            AppendNode(right, e.Right, parameters, queryParams, json);

            builder.Append($"({left.ToString()} {op} {right.ToString()})");
        }

        protected void AppendEqualLikeNodes(StringBuilder builder, Expression node, ReadOnlyCollection<ParameterExpression> parameters, DynamicParameters queryParams, string json)
        {
            BinaryExpression e = node as BinaryExpression;

            StringBuilder left = new StringBuilder();
            StringBuilder right = new StringBuilder();

            AppendNode(left, e.Left, parameters, queryParams, json);
            AppendNode(right, e.Right, parameters, queryParams, json);

            MemberExpression member = e.Left as MemberExpression;
            if (member != null && member.Member.MemberType == MemberTypes.Property)
            {
                PropertyInfo pi = member.Member as PropertyInfo;
                if (pi.PropertyType == typeof(string))
                {
                    builder.Append($"({left.ToString()} LIKE '%' || {right.ToString()} || '%')");
                    return;
                }
            }
            builder.Append($"({left.ToString()} = {right.ToString()})");
        }

        protected bool GetMemberNodes(StringBuilder builder, Expression node, ReadOnlyCollection<ParameterExpression> parameters, DynamicParameters queryParams, Stack<string> nodes)
        {
            MemberExpression member = node as MemberExpression;
            nodes.Push(GetMemberName(member.Member));

            Expression parent = member.Expression as Expression;
            while (parent != null)
            {
                switch (parent.NodeType)
                {
                    case ExpressionType.MemberAccess:
                        member = parent as MemberExpression;
                        nodes.Push($"{GetMemberName(member.Member)}.");
                        parent = member.Expression;
                        break;

                    case ExpressionType.Constant:
                        ////log.Info($"Member Constant Node {node.NodeType} ({node.GetType()}) {parent.NodeType} ({parent.GetType()})");
                        LambdaExpression fn = Expression.Lambda(node);
                        object val = fn.Compile().DynamicInvoke();
                        AppendNode(builder, Expression.Constant(val), parameters, queryParams);
                        return false;

                    case ExpressionType.Call:
                        MethodCallExpression call = (MethodCallExpression)parent;
                        if (call.Method.Name == "get_Item")
                        {
                            // this is an indexing op
                            member = call.Object as MemberExpression;
                            ConstantExpression c = call.Arguments[0] as ConstantExpression;
                            nodes.Push($"{GetMemberName(member.Member)}[{c.Value}].");
                            parent = member.Expression;
                        }
                        else
                            parent = null;
                        break;

                    default:
                        ////log.Info($"MemberAccess Node Type {parent.NodeType} {parent.ToString()}");
                        parent = null;
                        break;
                }
            }
            return true;
        }

        protected void AppendMember(StringBuilder builder, Expression node, ReadOnlyCollection<ParameterExpression> parameters, DynamicParameters queryParams, string json)
        {
            MemberExpression member = node as MemberExpression;
            if (member == null)
                return;

            Stack<string> nodes = new Stack<string>();

            if (GetMemberNodes(builder, node, parameters, queryParams, nodes))
            {
                builder.Append($"json_extract({json}, '$.");
                foreach (string s in nodes)
                    builder.Append(s);
                builder.Append("')");
            }
        }

        protected void AppendExistsInListNode(StringBuilder builder, Expression node, ReadOnlyCollection<ParameterExpression> parameters, DynamicParameters queryParams, string json)
        {
            MethodCallExpression callNode = (MethodCallExpression)node;

            // first param should be the list, second is the predicate
            MemberExpression member = callNode.Arguments[0] as MemberExpression;
            if (member == null)
                return;

            Stack<string> nodes = new Stack<string>();

            if (GetMemberNodes(builder, member, parameters, queryParams, nodes))
            {
                StringBuilder phrase = new StringBuilder("EXISTS(SELECT * FROM json_each(jsonData, '$.");
                foreach (string s in nodes)
                    phrase.Append(s);
                phrase.Append("') WHERE ");

                // predicate
                AppendNode(phrase, callNode.Arguments[1], parameters, queryParams, "[value]");

                phrase.Append(")");

                builder.Append(phrase.ToString());
            }

            // this is something we need to convert to json calls
            // EXISTS(SELECT * FROM json_each(jsonData, '$.stringStats')
            //    WHERE json_extract([value], '$.key') = 1 and json_extract([value], '$.value') LIKE '%Apple%')
        }

        protected static string GetMemberName(MemberInfo member)
        {
            JsonPropertyAttribute jpa = member.GetCustomAttribute<JsonPropertyAttribute>();
            if (jpa != null)
                return jpa.PropertyName;
            return member.Name;
        }

        protected static bool EvaluateTest(Expression expression)
        {
            LambdaExpression fn = Expression.Lambda(expression);
            object val = fn.Compile().DynamicInvoke();
            return Convert.ToBoolean(val);
        }

        #endregion

    }

    public class SQLiteSandboxDatabase<T, CE> : SQLiteContentDatabase<T>, IGenericSandboxContentProvider<T> where T : class, IMetadata where CE : ChangeEntry<T>, new()
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SQLiteSandboxDatabase(string connectionName, string tableName, Func<T, uint> idfn) : base(connectionName, tableName, idfn)
        {
        }

        protected override void OnInitialize(DbConnection connection)
        {
            connection.Execute($@"
				CREATE TABLE IF NOT EXISTS {TableName} (
				id INT,
				userId BINARY(16),
				lastModified DATETIME,
				userModified VARCHAR(100) COLLATE NOCASE,
				jsonData TEXT COLLATE NOCASE,
				PRIMARY KEY(id, userId));");
        }

        #region IGenericContentProvider

        public override bool Delete(uint id)
        {
            // no-op
            return false;
        }

        public override IQueryable<T> Get()
        {
            return ContentProviderT?.Get();
        }

        public override T Get(uint id)
        {
            return ContentProviderT?.Get(id);
        }

        public override bool Save(T item)
        {
            return ContentProviderT?.Save(item) == true;
        }

        public override bool Update(T item)
        {
            return ContentProviderT?.Update(item) == true;
        }

        public override IQueryable<T> Search(Expression<Func<T, bool>> criteria)
        {
            return ContentProviderT?.Search(criteria);
        }

        #endregion

        #region IGenericSandboxContentProvider

        public IGenericContentProvider ContentProvider { get; set; }

        public ChangeEntry GetChangeEntry(Guid userId, uint id)
        {
            return GetChange(userId, id);
        }

        public IEnumerable<ChangeEntry> GetChangeEntries()
        {
            return GetChanges();
        }

        public IEnumerable<ChangeEntry> GetChangeEntries(Guid userId)
        {
            return GetChanges(userId);
        }

        public void UpdateChange(Guid userId, ChangeEntry entry)
        {
            ////entry.SubmissionTime = DateTime.Now;

            string content = JsonConvert.SerializeObject(entry);

            using (DbConnection db = GetConnection())
            {
                db.Execute($@"
						INSERT INTO {TableName} (id, userId, lastModified, userModified, jsonData)
						VALUES (@id, @userId, @LastModified, @ModifiedBy, @content)
						ON CONFLICT (id, userId) DO UPDATE SET
						lastModified = @LastModified, userModified = @ModifiedBy, jsonData = @content
						WHERE id = @id AND userId = @userId;
						", new
                {
                    id = entry.EntryId,
                    userId,
                    entry.Metadata.LastModified,
                    entry.Metadata.ModifiedBy,
                    content
                });
            }
        }

        public void AcceptChange(Guid userId, ChangeEntry entry)
        {
            AcceptChange(userId, ((CE)entry).Entry);
        }

        public void DeleteChange(Guid userId, ChangeEntry entry)
        {
            DeleteChange(userId, ((CE)entry).Entry);
        }

        public IGenericContentProvider<T> ContentProviderT => ContentProvider as IGenericContentProvider<T>;

        public void AcceptChange(Guid userId, T change)
        {
            ContentProviderT?.Update(change);
        }

        public void DeleteChange(Guid userId, T change)
        {
            try
            {
                using (DbConnection db = GetConnection())
                    db.Execute($"DELETE FROM {TableName} WHERE id=@id AND userId=@userId", new { id = GetId(change), userId });
            }
            catch (DbException)
            {
            }
        }

        public ChangeEntry<T> GetChange(Guid userId, uint id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    string content = db.ExecuteScalar<string>($"SELECT jsonData FROM {TableName} WHERE id=@id AND userId=@userId", new { id, userId });
                    if (string.IsNullOrEmpty(content))
                        return null;

                    return JsonConvert.DeserializeObject<CE>(content);
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public IEnumerable<ChangeEntry<T>> GetChanges()
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    var query = db.Query<string>($"SELECT jsonData FROM {TableName}");
                    return query.Select(content => JsonConvert.DeserializeObject<CE>(content)).AsQueryable();
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public IEnumerable<ChangeEntry<T>> GetChanges(Guid userId)
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    var query = db.Query<string>($"SELECT jsonData FROM {TableName} WHERE userId=@userId", new { userId });
                    return query.Select(content => JsonConvert.DeserializeObject<CE>(content)).AsQueryable();
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public void UpdateChange(Guid userId, T entry)
        {
            try
            {
                ChangeEntry<T> change = GetChange(userId, GetId(entry));
                if (change == null)
                {
                    var user = AuthProviderHost.PrimaryAuthProvider.GetAccount(null, userId.ToString());

                    change = new CE();
                    change.UserName = user.Name;
                    change.UserGuid = user.AccountGuid;
                }
                change.Entry = entry;

                UpdateChange(userId, change);

            }
            catch (DbException ex)
            {
            }
        }

        public IQueryable<T> Search(Guid userId, Expression<Func<T, bool>> criteria)
        {
            if (criteria == null)
                return Enumerable.Empty<T>().AsQueryable();

            Expression node = criteria.Body;

            if (node.CanReduce)
                node = node.Reduce();

            DynamicParameters queryParams = new DynamicParameters();
            queryParams.Add("@userId", userId);

            StringBuilder query = new StringBuilder($"SELECT jsonData FROM {TableName} WHERE userId=@userId AND ");

            AppendNode(query, criteria, criteria.Parameters, queryParams);

            ////log.Debug($"Attempting query: {query.ToString()}");

            try
            {
                using (DbConnection db = GetConnection())
                {
                    var results = db.Query<string>(query.ToString(), queryParams);
                    return Queryable.Union(
                        results.Select(content => JsonConvert.DeserializeObject<CE>(content).Entry).AsQueryable(),
                        ContentProviderT?.Search(criteria));
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        #endregion
    }
}
