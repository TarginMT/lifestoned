﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Lifestoned.DataModel.Shared;

namespace Lifestoned.DataModel.Gdle.Spells
{
    public class SpellTableEntry : IMetadata
    {
        [JsonProperty("key")]
        public uint Key { get; set; }

        [JsonProperty("value")]
        public Spell Value { get; set; }

        #region Metadata

        [JsonProperty("lastModified")]
        [Display(Name = "Last Modified Date")]
        public DateTime? LastModified { get; set; }

        [JsonProperty("modifiedBy")]
        [Display(Name = "Last Modified By")]
        public string ModifiedBy { get; set; }

        [JsonProperty("changelog")]
        public List<ChangelogEntry> Changelog { get; set; } = new List<ChangelogEntry>();

        [JsonProperty("userChangeSummary")]
        public string UserChangeSummary { get; set; }

        [JsonProperty("isDone")]
        [Display(Name = "Is Done")]
        public bool IsDone { get; set; }

        #endregion
    }

    public class SpellTableChange : ChangeEntry<SpellTableEntry>
    {
        public const string TypeName = "spells";

        [JsonIgnore]
        public override uint EntryId
        {
            get => Entry.Key;
            set => Entry.Key = value;
        }

        [JsonIgnore]
        public override string EntryType => SpellTableChange.TypeName;

        [JsonProperty("spell")]
        public override SpellTableEntry Entry { get; set; }
    }

    public class Spell
    {
        [JsonProperty("base_mana")]
        public int BaseMana { get; set; }

        [JsonProperty("base_range_constant")]
        public float BaseRange { get; set; }

        [JsonProperty("base_range_mod")]
        public float BaseRangeMod { get; set; }

        [JsonProperty("bitfield")]
        public uint Flags { get; set; }

        [JsonProperty("caster_effect")]
        public uint CasterEffect { get; set; }

        [JsonProperty("category")]
        public uint Category { get; set; }

        [JsonProperty("component_loss")]
        public float ComponentLoss { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }

        [JsonProperty("display_order")]
        public uint DisplayOrder { get; set; }

        [JsonProperty("fizzle_effect")]
        public uint FizzleEffect { get; set; }

        [JsonProperty("formula")]
        public int[] Formula { get; set; } = new int[8];

        [JsonProperty("formula_version")]
        public uint FormulaVersion { get; set; }

        [JsonProperty("iconID")]
        public uint IconId { get; set; }

        [JsonProperty("mana_mod")]
        public uint ManaMod { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("non_component_target_type")]
        public uint NonComponentTargetType { get; set; }

        [JsonProperty("power")]
        public uint Power { get; set; }

        [JsonProperty("recovery_amount")]
        public float RecoveryAmount { get; set; }

        [JsonProperty("recovery_interval")]
        public double RecoveryInterval { get; set; }

        [JsonProperty("school")]
        public uint School { get; set; }

        [JsonProperty("spell_economy_mod")]
        public float SpellEconomyMod { get; set; }

        [JsonProperty("target_effect")]
        public uint TargetEffect { get; set; }

        [JsonProperty("meta_spell")]
        public MetaSpell MetaSpell { get; set; }

    }

    public class MetaSpell
    {
        [JsonProperty("sp_type")]
        public int Type { get; set; }

        [JsonProperty("spell")]
        public MetaSpellBase Spell { get; set; }
    }

    public class MetaSpellBase
    {
        [JsonProperty("spell_id")]
        public uint Id { get; set; }

        // enchantment, projectile-enchantment
        [JsonProperty("degrade_limit", NullValueHandling = NullValueHandling.Ignore)]
        public float? Degrade { get; set; } = null;

        // enchantment, projectile-enchantment
        [JsonProperty("degrade_modifier", NullValueHandling = NullValueHandling.Ignore)]
        public float? DegradeMod { get; set; } = null;

        // enchantment, projectile-enchantment
        [JsonProperty("duration", NullValueHandling = NullValueHandling.Ignore)]
        public double? Duration { get; set; } = null;

		// enchantment, projectile-enchantment
        [JsonProperty("smod", NullValueHandling = NullValueHandling.Ignore)]
        public StatMod Mod { get; set; } = null;

        // enchantment, projectile-enchantment
        [JsonProperty("spellCategory", NullValueHandling = NullValueHandling.Ignore)]
        public int? Category { get; set; } = null;

        // enchantment
        [JsonProperty("dtype", NullValueHandling = NullValueHandling.Ignore)]
        public int? dtype { get; set; } = null;


        // projectile
        [JsonProperty("etype", NullValueHandling = NullValueHandling.Ignore)]
        public int? etype { get; set; } = null;

        [JsonProperty("baseIntensity", NullValueHandling = NullValueHandling.Ignore)]
        public int? baseIntensity { get; set; } = null;

        [JsonProperty("variance", NullValueHandling = NullValueHandling.Ignore)]
        public float? variance { get; set; } = null;

        [JsonProperty("wcid", NullValueHandling = NullValueHandling.Ignore)]
        public int? wcid { get; set; } = null;

        [JsonProperty("numProjectiles", NullValueHandling = NullValueHandling.Ignore)]
        public int? numProjectiles { get; set; } = null;

        [JsonProperty("numProjectilesVariance", NullValueHandling = NullValueHandling.Ignore)]
        public float? numProjectilesVariance { get; set; } = null;

        [JsonProperty("spreadAngle", NullValueHandling = NullValueHandling.Ignore)]
        public float? spreadAngle { get; set; } = null;

        [JsonProperty("verticalAngle", NullValueHandling = NullValueHandling.Ignore)]
        public float? verticalAngle { get; set; } = null;

        [JsonProperty("defaultLaunchAngle", NullValueHandling = NullValueHandling.Ignore)]
        public float? defaultLaunchAngle { get; set; } = null;

        [JsonProperty("bNonTracking", NullValueHandling = NullValueHandling.Ignore)]
        public int? bNonTracking { get; set; } = null;

        [JsonProperty("createOffset", NullValueHandling = NullValueHandling.Ignore)]
        public XYZ createOffset { get; set; } = null;

        [JsonProperty("padding", NullValueHandling = NullValueHandling.Ignore)]
        public XYZ padding { get; set; } = null;

        [JsonProperty("dims", NullValueHandling = NullValueHandling.Ignore)]
        public XYZ dims { get; set; } = null;

        [JsonProperty("peturbation", NullValueHandling = NullValueHandling.Ignore)]
        public XYZ peturbation { get; set; } = null;

        [JsonProperty("imbuedEffect", NullValueHandling = NullValueHandling.Ignore)]
        public int? imbuedEffect { get; set; } = null;

        [JsonProperty("slayerCreatureType", NullValueHandling = NullValueHandling.Ignore)]
        public int? slayerCreatureType { get; set; } = null;

        [JsonProperty("slayerDamageBonus", NullValueHandling = NullValueHandling.Ignore)]
        public float? slayerDamageBonus { get; set; } = null;

        [JsonProperty("critFreq", NullValueHandling = NullValueHandling.Ignore)]
        public double? critFreq { get; set; } = null;

        [JsonProperty("critMultiplier", NullValueHandling = NullValueHandling.Ignore)]
        public double? critMultiplier { get; set; } = null;

        [JsonProperty("ignoreMagicResist", NullValueHandling = NullValueHandling.Ignore)]
        public int? ignoreMagicResist { get; set; } = null;

        [JsonProperty("elementalModifier", NullValueHandling = NullValueHandling.Ignore)]
        public double? elementalModifier { get; set; } = null;

        // projectile-life
        [JsonProperty("drain_percentage", NullValueHandling = NullValueHandling.Ignore)]
        public float? drain_percentage { get; set; } = null;
        [JsonProperty("damage_ratio", NullValueHandling = NullValueHandling.Ignore)]
        public float? damage_ratio { get; set; } = null;

        // boost
        [JsonProperty("dt", NullValueHandling = NullValueHandling.Ignore)]
        public int? dt { get; set; } = null;
        [JsonProperty("boost", NullValueHandling = NullValueHandling.Ignore)]
        public int? boost { get; set; } = null;
        [JsonProperty("boostVariance", NullValueHandling = NullValueHandling.Ignore)]
        public int? boostVariance { get; set; } = null;

        // transfer
        [JsonProperty("src", NullValueHandling = NullValueHandling.Ignore)]
        public int? src { get; set; } = null;
        [JsonProperty("dest", NullValueHandling = NullValueHandling.Ignore)]
        public int? dest { get; set; } = null;
        [JsonProperty("proportion", NullValueHandling = NullValueHandling.Ignore)]
        public float? proportion { get; set; } = null;
        [JsonProperty("lossPercent", NullValueHandling = NullValueHandling.Ignore)]
        public float? lossPercent { get; set; } = null;
        [JsonProperty("sourceLoss", NullValueHandling = NullValueHandling.Ignore)]
        public int? sourceLoss { get; set; } = null;
        [JsonProperty("transferCap", NullValueHandling = NullValueHandling.Ignore)]
        public int? transferCap { get; set; } = null;
        [JsonProperty("maxBoostAllowed", NullValueHandling = NullValueHandling.Ignore)]
        public int? maxBoostAllowed { get; set; } = null;
        [JsonProperty("bitfield", NullValueHandling = NullValueHandling.Ignore)]
        public int? bitfield { get; set; } = null;

        // portal recall/link
        [JsonProperty("index", NullValueHandling = NullValueHandling.Ignore)]
        public int? index { get; set; } = null;

        // portal sending
        [JsonProperty("pos", NullValueHandling = NullValueHandling.Ignore)]
        public Position pos { get; set; } = null;

        // dispell
        [JsonProperty("min_power", NullValueHandling = NullValueHandling.Ignore)]
        public int? min_power { get; set; } = null;
        [JsonProperty("max_power", NullValueHandling = NullValueHandling.Ignore)]
        public int? max_power { get; set; } = null;
        [JsonProperty("power_variance", NullValueHandling = NullValueHandling.Ignore)]
        public float? power_variance { get; set; } = null;
        [JsonProperty("school", NullValueHandling = NullValueHandling.Ignore)]
        public int? school { get; set; } = null;
        [JsonProperty("align", NullValueHandling = NullValueHandling.Ignore)]
        public int? align { get; set; } = null;
        [JsonProperty("number", NullValueHandling = NullValueHandling.Ignore)]
        public int? number { get; set; } = null;
        [JsonProperty("number_variance", NullValueHandling = NullValueHandling.Ignore)]
        public float? number_variance { get; set; } = null;

        // portal summon
        [JsonProperty("portal_lifetime", NullValueHandling = NullValueHandling.Ignore)]
        public double? portal_lifetime { get; set; } = null;
        [JsonProperty("link", NullValueHandling = NullValueHandling.Ignore)]
        public int? link { get; set; } = null;
    }

    public class StatMod
    {
        [JsonProperty("key")]
        public uint Key { get; set; }

        [JsonProperty("type")]
        public uint Type { get; set; }

        [JsonProperty("val")]
        public float Value { get; set; }

    }

	public class SpellsFile
    {
		public class SpellsHashTable
        {
            [JsonProperty("spellBaseHash")]
            public List<SpellTableEntry> Entries { get; set; } = new List<SpellTableEntry>();
        }

        [JsonProperty("table")]
        public SpellsHashTable Table { get; set; } = new SpellsHashTable();
    }
}
