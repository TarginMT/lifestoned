﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-body-part-armor', {
    props: ['armor', 'floats'],
    model: { prop: 'armor', event: 'changed' },
    methods: {
        calcArmor(v) {
            let q = this.floats.find((x) => x.Key === v);
            let r = q ? q.Value : 0;
            return Math.floor(r * this.armor.BaseArmor);
        }
    },
    watch: {
        'armor.BaseArmor': function (v, o) {
            this.armor.ArmorVsBludgeon = this.calcArmor(15);
            this.armor.ArmorVsPierce = this.calcArmor(14);
            this.armor.ArmorVsSlash = this.calcArmor(13);
            this.armor.ArmorVsAcid = this.calcArmor(18);
            this.armor.ArmorVsCold = this.calcArmor(16);
            this.armor.ArmorVsFire = this.calcArmor(17);
            this.armor.ArmorVsElectric = this.calcArmor(19);
            this.armor.ArmorVsNether = this.calcArmor(165);
        }
    },
    template: `
    <div>
        <div class="row row-spacer">
            <div class="col-md-2">Base</div>
            <div class="col-md-2">Blunt</div>
            <div class="col-md-2">Pierce</div>
            <div class="col-md-2">Slash</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2"><input v-model.number="armor.BaseArmor" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="armor.ArmorVsBludgeon" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.ArmorVsPierce" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.ArmorVsSlash" type="text" class="form-control" readonly /></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2">Acid</div>
            <div class="col-md-2">Cold</div>
            <div class="col-md-2">Fire</div>
            <div class="col-md-2">Lightning</div>
            <div class="col-md-2">Nether</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2"><input v-model.number="armor.ArmorVsAcid" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.ArmorVsCold" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.ArmorVsFire" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.ArmorVsElectric" type="text" class="form-control" readonly /></div>
            <div class="col-md-2"><input v-model.number="armor.ArmorVsNether" type="text" class="form-control" readonly /></div>

        </div>

    </div>
    `
});

Vue.component('lsd-body-part-target', {
    props: ['part'],
    model: { prop: 'part', event: 'changed' },
    template: `
    <div>
        <hr />
        <div class="row row-spacer">
            <div class="col-md-2"></div>
            <div class="col-md-2">Front Left</div>
            <div class="col-md-2">Front Right</div>
            <div class="col-md-2">Back Left</div>
            <div class="col-md-2">Back Right</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2">High</div>
            <div class="col-md-2"><input v-model.number="part.HLF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.HRF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.HLB" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.HRB" type="text" class="form-control" /></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2">Mid</div>
            <div class="col-md-2"><input v-model.number="part.MLF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.MRF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.MLB" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.MRB" type="text" class="form-control" /></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-2">Low</div>
            <div class="col-md-2"><input v-model.number="part.LLF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.LRF" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.LLB" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.LRB" type="text" class="form-control" /></div>
        </div>
    </div>
    `
});

Vue.component('lsd-body-part', {
    props: ['kvp', 'floats'],
    model: { prop: 'kvp', event: 'changed' },
    computed: {
        part() {
            return this.kvp.BodyPart;
        }
    },
    methods: {
        deleted() {
            this.$emit('deleted', this.kvp);
        }
    },
    template: `
    <div>
        <div class="row row-spacer">
            <div class="col-md-3">Damage Type</div>
            <div class="col-md-2">Damage</div>
            <div class="col-md-2">Variance</div>
            <div class="col-md-2">Height</div>
            <div class="col-md-offset-2 col-md-1">
                <button @click="deleted" type="button" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-3"><lsd-enum-select type="DamageType" v-model="part.DType" keyOnly></lsd-enum-select></div>
            <div class="col-md-2"><input v-model.number="part.DVal" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.DVar" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model.number="part.BH" type="text" class="form-control" /></div>

        </div>

        <lsd-body-part-armor v-model="part.ArmorValues" :floats="floats"></lsd-body-part-armor>
        <lsd-body-part-target v-model="part.SD"></lsd-body-part-target>
    </div>
    `
});

Vue.component('lsd-body-part-list', {
    props: ['body', 'floats'],
    model: { prop: 'body', event: 'changed' },
    data() {
        return {
            newPart: null
        };
    },
    computed: {
        parts() {
            return this.body ? this.body.BodyParts : [];
        }
    },
    methods: {
        openDialog() {
            this.$refs.modal.show();
        },
        openImport() {
            this.$refs.import.show();
        },
        addNew() {
            if (this.newPart) {
                if (!this.body) this.body = { BodyParts: [] };

                var key = parseInt(this.newPart.Key);
                this.body.BodyParts.push({ Key: key, BodyPart: this.$weenie.newBodyPart() });
            }
        },
        importWeenie(weenie) {
            if (weenie && weenie.Body) {
                this.$emit('changed', weenie.Body);
            }
        },
        deleted(part) {
            var idx = this.body.BodyParts.indexOf(part);
            if (idx >= 0) {
                this.body.BodyParts.splice(idx, 1);
            }
        }
    },
    template: `
    <lsd-panel title="Body Parts" showAdd>

        <template v-slot:headerCommands>
        <div class="col-md-offset-4 col-md-2">
            <button @click="openImport" type="button" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-import"></i></button>
            <button @click="openDialog" type="button" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        </template>
        
        <ul class="nav nav-stacked col-md-2">
            <li v-for="(part, idx) in parts" :key="part.Key" :class="{ 'active' : (idx == 0) }"><a v-lsd-enum-display="{ type: 'BodyPartType', key: part.Key }" :href="'#w_bp_'+idx" data-toggle="tab">{{ part.Key }}</a></li>
        </ul>
        <div class="tab-content col-md-10">
            <div v-for="(part, idx) in parts" :id="'w_bp_'+idx" class="tab-pane" :class="{ 'active' : (idx == 0) }">
                <lsd-body-part v-model="parts[idx]" :floats="floats" @deleted="deleted"></lsd-body-part>
            </div>
        </div>

        <lsd-dialog ref="modal" title="Add Body Part" @saved="addNew">
            <lsd-enum-select type="BodyPartType" v-model="newPart"></lsd-enum-select>
        </lsd-dialog>

        <lsd-weenie-import-dialog ref="import" title="Import Body Part" @changed="importWeenie"></lsd-weenie-import-dialog>

    </lsd-panel>
    `
});

// bodypart     damage  variance    height  damagetype
// ---------------------------------------------------
// armor
// ---------------------------------------------------
// targeting
// ---------------------------------------------------