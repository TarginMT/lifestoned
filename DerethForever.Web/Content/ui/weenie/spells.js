﻿
/// <reference path="../../../Scripts/vue.js" />

let spell_cache = [];
function getSpellName(id) {
    if (spell_cache.length === 0) {
        // we need to sleep and try again
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                resolve(getSpellName(id));
            }, 500);
        });
    } else {
        return new Promise(function (resolve, reject) {
            var item = spell_cache.find(function (v, i, a) {
                return v.id === id;
            });

            if (item) resolve(item.name);
            else reject('<not found>');
        });
    }
}

Vue.directive('lsd-spell-display', {
    bind: function (el, binding, vnode) {
        var $el = $(el);
        var id = parseInt(el.dataset.spell);

        if (id) {
            getSpellName(id).then(function (name) {
                $el.val(name);
            });
        }
    }
});

Vue.component('lsd-spells-selector', {
    data() {
        return {
            search: null,
            school: 0,
            spellList: [],
            selectedSpells: []
        };
    },
    methods: {
        open() {
            this.$refs.modal.show();
        },
        addSpells() {
            this.$emit('changed', this.selectedSpells);
        },
        updateList() {
            if (spell_cache.length === 0) {
                var ls = localStorage.getItem('spells_list');
                if (ls) {
                    spell_cache = JSON.parse(ls);
                    this.spellList = spell_cache;
                } else {
                    var $this = this;
                    $.getJSON('/content/spells.json', function (data) {
                        localStorage.setItem('spells_list', JSON.stringify(data));
                        $this.spellList = data;
                        spell_cache = data;
                    });
                }
            } else {
                this.spellList = spell_cache;
            }
        }
    },
    computed: {
        filteredSpells() {
            var school = parseInt(this.school);
            var search = this.search;
            var searchTest = !(search && search.length > 2);
            return this.spellList.filter(function (v, i, a) {
                var matchSchool = school === 0 || v.school === school;
                return matchSchool && (searchTest || v.name.includes(search));
            });
        }
    },
    created() {
        this.updateList();
    },
    template: `
    <lsd-dialog ref="modal" title="Add Spells" @saved="addSpells">
        <div class="row">
            <div class="col-xs-8"><input v-model="search" type="text" placeholder="Search..." /></div>
            <div class="col-xs-4">
                <i class="glyphicon glyphicon-filter"></i>
                <select id="schools" v-model="school">
                    <option value="0">All</option>
                    <option value="1">War</option>
                    <option value="2">Life</option>
                    <option value="3">Item</option>
                    <option value="4">Creature</option>
                </select>
            </div>
        </div>
        <table class="table table-bordered table-condensed table-hover">
            <thead>
                <tr>
                    <th></th><th data-prop="id">Id</th><th data-prop="name">Name</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in filteredSpells">
                    <td><input type="checkbox" :value="item.id" v-model="selectedSpells" /></td>
                    <td>{{ item.id }}</td>
                    <td>{{ item.name }}</td>
                </tr>
            </tbody>
        </table>
    </lsd-dialog>
    `
});

Vue.component('lsd-item-spell-table', {
    props: ['spells'],
    model: { prop: 'spells', event: 'changed' },
    methods: {
        deleted(spell) {
            var idx = this.spells.indexOf(spell);
            if (idx >= 0) {
                this.spells.splice(idx, 1);
            }
        },
        addNew() {
            this.$refs.selector.open();
        },
        addSpells(items) {
            var spells = this.spells || [];

            for (var item of items) {
                var exists = spells.find(function (v, i, a) { return v.SpellId === item; });
                if (!exists) {
                    spells.push({
                        SpellId: item,
                        Deleted: false,
                        Stats: {
                            CastingChance: null
                        }
                    });
                }
            }
            if (spells !== this.spells)
                this.$emit('changed', spells);
        }
    },
    template: `
    <lsd-panel title="Spell Table" showAdd @adding="addNew">
        <lsd-item-spell-table-header showDelete></lsd-item-spell-table-header>
        <lsd-item-spell v-for="(spell, idx) in spells" :key="spell.SpellId" v-model="spells[idx]" showDelete @deleted="deleted"></lsd-item-spell>

        <lsd-spells-selector ref="selector" @changed="addSpells"></lsd-spells-selector>
    </lsd-panel>
    `
});

Vue.component('lsd-item-spell-table-header', {
    props: {
        showDelete: { type: Boolean, default: false }
    },
    template: `
    <div class="row row-spacer">
        <div class="col-md-2">Id</div>
        <div class="col-md-4">Name</div>
        <div class="col-md-2">Probability</div>
        <div v-if="showDelete" class="col-md-1"></div>
    </div>
    `
});

Vue.component('lsd-item-spell', {
    props: {
        'spell': { type: Object },
        showDelete: { type: Boolean, default: false }
    },
    model: { prop: 'spell', event: 'changed' },
    methods: {
        deleted() {
            this.$emit('deleted', this.spell);
        }
    },
    template: `
    <div class="row row-spacer">
        <div class="col-md-2"><input v-model="spell.SpellId" class="form-control" readonly /></div>
        <div class="col-md-4"><input v-lsd-spell-display :data-spell="spell.SpellId" class="form-control" readonly /></div>
        <div class="col-md-2"><input v-model="spell.Stats.CastingChance" class="form-control" /></div>
        <div v-if="showDelete" class="col-md-offset-3 col-md-1"><button @click="deleted" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i></button></div>
    </div>
    `
});
