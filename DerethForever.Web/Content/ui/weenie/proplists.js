﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-prop-add', {
    props: ['name', 'type'],
    data: function () { return { selected: null }; },
    methods: {
        addNew: function () {
            this.$emit('added', this.selected);
        }
    },
    template: `
    <div class="panel-footer clearfix">
        <div class="col-md-3">Add New {{ name }}</div>
        <div class="col-md-4">
            <lsd-enum-select v-model="selected" :type="type" keyOnly></lsd-enum-select>
        </div>
        <div class="col-md-1">
            <button class="btn btn-sm btn-primary" v-on:click.prevent="addNew"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
    </div>
    `
});

Vue.component('lsd-prop-delete', {
    template: `
    <div class="col-md-1">
        <button type="button" class="btn btn-xs btn-danger" v-on:click.prevent="$emit('click')"><i class="glyphicon glyphicon-trash"></i></button>
    </div>
    `
});

Vue.component('lsd-props-base', {
    props: {
        vals: { type: Array },
        name: { type: String },
        type: { type: String },
        width: { type: Number },
        active: { type: Boolean },
        newfn: { type: Function, default: null }
    },
    model: { prop: 'vals', event: 'changed' },
    methods: {
        addNew(event) {
            if (this.newfn) {
                this.vals.push(this.newfn(event));
            } else {
                this.vals.push({
                    Key: event,
                    Value: null
                });
            }
        },
        removed(idx) {
            this.vals.splice(idx, 1);
        }
    },
    template: `
    <div :class="(active) ? 'tab-pane fade in active' : 'tab-pane fade'">
        <div class="panel-body">
            <!-- lsd-table :items="vals">
                <lsd-table-col style="width: 25rem;" title="Key" bind-key="Key" read-only v-slot="{ item }">
                    <span v-lsd-enum-display="{'type': type, 'key': item ? item.Key : -1 }"></span>
                </lsd-table-col>
                <lsd-table-col title="Value" bind-key="Value"></lsd-table-col>
                <lsd-table-col style="width: 1.5rem;" bind-key="Key" read-only v-slot="{ item }">
                    <lsd-prop-delete @click="removed(item.Key)"></lsd-prop-delete>
                </lsd-table-col>
            </lsd-table -->

            <div v-for="(val, idx) in vals" :key="$hash(val)" class="row row-spacer">
                <div v-lsd-enum-display="{'type': type, 'key': val.Key}" class="col-md-3"></div>
                <div class="col-md-8">
                    <slot :item="val">
                    <input v-model="val.Value" type="text" class="form-control" />
                    </slot>
                </div>
                <lsd-prop-delete @click="removed(idx)"></lsd-prop-delete>
            </div>
        </div>
        <lsd-prop-add :name="name" :type="type" @added="addNew"></lsd-prop-add>
    </div>
    `
});

Vue.component('lsd-bool-props', {
    props: ['vals'],
    model: { prop: 'vals', event: 'changed' },
    template: `
    <lsd-props-base v-model="vals" name="Bool" type="BoolPropertyId">
        <input slot-scope="p" v-model="p.item.Value" type="checkbox" class="form-control" />
    </lsd-props-base>
    `
});

Vue.component('lsd-did-props', {
    props: ['vals'],
    model: { prop: 'vals', event: 'changed' },
    template: `
    <lsd-props-base v-model="vals" name="Data ID" type="DidPropertyId">
    </lsd-props-base>
    `
});


Vue.component('lsd-double-props', {
    props: ['vals'],
    model: { prop: 'vals', event: 'changed' },
    template: `
    <lsd-props-base v-model="vals" name="Float" type="DoublePropertyId">
    </lsd-props-base>
    `
});

Vue.component('lsd-iid-props', {
    props: ['vals'],
    model: { prop: 'vals', event: 'changed' },
    template: `
    <lsd-props-base v-model="vals" name="Instance ID" type="IidPropertyId">
    </lsd-props-base>
    `
});

Vue.component('lsd-int-props', {
    props: ['vals'],
    model: { prop: 'vals', event: 'changed' },
    template: `
    <lsd-props-base v-model="vals" name="Int" type="IntPropertyId">
        <template slot-scope="p">
        <lsd-enum-select v-if="p.item.Key==1" type="ItemType" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-enum-select v-else-if="p.item.Key==2" type="CreatureType" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-enum-select v-else-if="p.item.Key==3" type="PaletteTemplate" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-flags-select v-else-if="p.item.Key==9" type="EquipMask" v-model="p.item.Value"></lsd-flags-select>
        <lsd-flags-select v-else-if="p.item.Key==10" type="EquipMask" v-model="p.item.Value"></lsd-flags-select>
        <lsd-flags-select v-else-if="p.item.Key==16" type="Usable" v-model="p.item.Value"></lsd-flags-select>
        <lsd-flags-select v-else-if="p.item.Key==18" type="UiEffects" v-model="p.item.Value"></lsd-flags-select>
        <lsd-enum-select v-else-if="p.item.Key==27" type="ArmorType" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-flags-select v-else-if="p.item.Key==45" type="DamageType" v-model="p.item.Value"></lsd-flags-select>
        <lsd-flags-select v-else-if="p.item.Key==46" type="CombatStyle" v-model="p.item.Value"></lsd-flags-select>
        <lsd-flags-select v-else-if="p.item.Key==47" type="AttackType" v-model="p.item.Value"></lsd-flags-select>
        <lsd-enum-select v-else-if="p.item.Key==48" type="SkillId" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-enum-select v-else-if="p.item.Key==131" type="Material" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-enum-select v-else-if="p.item.Key==158" type="WieldRequirements" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-enum-select v-else-if="p.item.Key==159" type="SkillId" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-enum-select v-else-if="p.item.Key==166" type="CreatureType" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-enum-select v-else-if="p.item.Key==188" type="HeritageGroup" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <lsd-flags-select v-else-if="p.item.Key==263" type="DamageType" mask="127" v-model="p.item.Value"></lsd-flags-select>
        <lsd-enum-select v-else-if="p.item.Key==353" type="WeaponType" v-model="p.item.Value" keyOnly></lsd-enum-select>
        <input v-else type="text" class="form-control" v-model.number="p.item.Value" />
        </template>
    </lsd-props-base>
    `
});

Vue.component('lsd-long-props', {
    props: ['vals'],
    model: { prop: 'vals', event: 'changed' },
    template: `
    <lsd-props-base v-model="vals" name="Int64" type="Int64PropertyId">
    </lsd-props-base>
    `
});


Vue.component('lsd-string-props', {
    props: ['vals', 'active'],
    model: { prop: 'vals', event: 'changed' },
    template: `
    <lsd-props-base v-model="vals" name="String" type="StringPropertyId" :active="active">
        <textarea slot-scope="p" v-model="p.item.Value" cols="110" rows="2" class="form-control"></textarea>
    </lsd-props-base>
    `
});
