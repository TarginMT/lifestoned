/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DerethForever.Web.Models.Discord;
using log4net;
using Lifestoned.DataModel.Gdle;
using Newtonsoft.Json;
using RestSharp;
using Lifestoned.DataModel.Shared;
using Lifestoned.DataModel.Account;
using Lifestoned.Providers;

namespace DerethForever.Web.Controllers
{
    public class WeenieController : BaseController
    {
        private const string _Session_IndexModel = "__weenieIndexModel";
        private const string _Session_DownloadModel = "__weenieDownloadModel";

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IWeenieProvider Provider => ContentProviderHost.GetProvider<IWeenieProvider>(WeenieChange.TypeName);

        private IWeenieSandboxProvider SandboxProvider => SandboxContentProviderHost.GetProvider<IWeenieSandboxProvider>(WeenieChange.TypeName);

        public IndexModel CurrentIndexModel
        {
            get { return (IndexModel)Session[_Session_IndexModel]; }
            set { Session[_Session_IndexModel] = value; }
        }

        public DownloadChangeModel CurrentDownload
        {
            get { return (DownloadChangeModel)Session[_Session_DownloadModel]; }
            set { Session[_Session_DownloadModel] = value; }
        }

        private Weenie GetWeenieForUser(uint id, string userGuid)
        {
            WeenieChange wc = null;
            ViewBag.UserGuid = null;
            string userToken = GetUserToken();

            if (!string.IsNullOrEmpty(userToken))
            {
                if (string.IsNullOrWhiteSpace(userGuid))
                {
                    Guid uid = new Guid(GetUserGuid());
                    // no userGuid specified, assume your own sandbox
                    ////wc = SandboxContentProviderHost.CurrentProvider.GetMySandboxedChange(GetUserToken(), id);
                    wc = SandboxProvider.GetChange(uid, id) as WeenieChange;
                }
                else
                {
                    // validate dev/admin or matches your id
                    if (User.IsInRole("Envoy") || GetUserGuid() == userGuid)
                        wc = SandboxProvider.GetChange(new Guid(userGuid), id) as WeenieChange;
                    ////wc = SandboxContentProviderHost.CurrentProvider.GetSandboxedChange(Guid.Parse(userGuid), id);
                    else
                        return null;
                }
            }

            Weenie weenie = null;
            if (wc != null)
            {
                weenie = wc.Entry;
                ViewBag.UserGuid = userGuid;
            }
            else
                weenie = Provider.Get(id);
            ////weenie = SandboxContentProviderHost.CurrentProvider.GetWeenie(GetUserToken(), id);

            return weenie;
        }

        [HttpGet]
        public ActionResult SpellList()
        {
            return View("SpellListPopup");
        }

        [HttpGet]
        public ActionResult Recent()
        {
            IndexModel model = CurrentIndexModel ?? new IndexModel();
            ////model.Results = SandboxContentProviderHost.CurrentProvider.RecentChanges(GetUserToken());

            model.Results = Provider.Search(w => w.LastModified >= DateTime.Now.Date.AddDays(-90), 100).OrderByDescending(w => w.LastModified)
                .Select(w => w.ToSearchResult()).ToList();

            return View(model);
        }

        ////[HttpGet]
        ////public ActionResult AllUpdates()
        ////{
        ////    var allUpdates = SandboxContentProviderHost.CurrentProvider.AllUpdates(GetUserToken());
        ////    var zipFile = new ZipFile();
        ////    foreach (var update in allUpdates)
        ////    {
        ////        // build the CachePwn json for this weenie
        ////        var weenie = SandboxContentProviderHost.CurrentProvider.GetWeenie(GetUserToken(), update.WeenieClassId);
        ////        // var cachePwn = Weenie.ConvertFromWeenie(weenie);
        ////        var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
        ////        var contents = JsonConvert.SerializeObject(weenie, Formatting.None, settings);

        ////        // build filename
        ////        var name = weenie.Name;
        ////        var removeChars = new List<string>() { "\t", "\n", "!", "\"" };
        ////        foreach (var removeChar in removeChars)
        ////            name = name.Replace(removeChar, "");

        ////        var filename = $"{update.WeenieClassId} - {name}.json";

        ////        // add json to zip file
        ////        zipFile.AddFile(filename, contents);
        ////    }

        ////    var bytes = zipFile.BuildZip();
        ////    string date = string.Format("{0:yyyy-MM-dd_hh-mm}", DateTime.Now);
        ////    return File(bytes, "application/zip", "GDLE-Latest-Updates-" + date + ".zip");
        ////}

        [HttpGet]
        public ActionResult Detail(uint id, string userGuid)
        {
            Weenie weenie = GetWeenieForUser(id, userGuid);

            if (weenie == null)
                return new HttpNotFoundResult();

            return View(weenie);
        }

        [HttpGet]
        public ActionResult Index()
        {
            IndexModel model = CurrentIndexModel ?? new IndexModel();
            BaseModel current = CurrentBaseModel;
            BaseModel.CopyBaseData(current, model);
            CurrentBaseModel = current;

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(IndexModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var criteria = ProcessSearchCriteria(model.Criteria);

                ////string token = GetUserToken();
                if (User.Identity.IsAuthenticated)
                {
                    Guid uid = new Guid(GetUserGuid());
                    model.Results = SandboxProvider.Search(uid, criteria).Select(w => w.ToSearchResult()).ToList();
                }
                else
                {
                    model.Results = Provider.Search(criteria).Select(w => w.ToSearchResult()).ToList();
                }

                ////model.Results = SandboxContentProviderHost.CurrentProvider.WeenieSearch(token, model.Criteria);

                ////if (!string.IsNullOrEmpty(token))
                ////{
                ////    List<WeenieChange> mine = SandboxContentProviderHost.CurrentProvider.GetMyWeenieChanges(GetUserToken());
                ////    model.Results.ForEach(w =>
                ////    {
                ////        if (mine.Any(m => m.Entry.WeenieClassId == w.WeenieClassId))
                ////            w.HasSandboxChange = true;
                ////    });
                ////}

                model.ShowResults = true;
            }
            catch (Exception ex)
            {
                model.Results = null;
                model.ShowResults = false;
                model.ErrorMessages.Add("Error retrieving data from the SandboxContentProviderHost");
                model.Exception = ex;
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Get(uint id, string userGuid)
        {
            Weenie weenie = GetWeenieForUser(id, userGuid);

            if (weenie == null)
                return new HttpNotFoundResult();

            SortTheThings(weenie);
            return JsonGet(weenie);
        }

        [HttpPut]
        [Authorize]
        public ActionResult Put(Weenie model)
        {
            ModelState.Clear();
            // TODO: Better Validation
            ActionResult result = ValidateWeenie(model);

            if (result != null)
            {
                ModelState.Clear();
                return ErrorResult("Validation Failed");
            }

            model.CleanDeletedAndEmptyProperties();
            model.LastModified = DateTime.Now;
            model.ModifiedBy = GetUserName();

            try
            {
                SandboxProvider.UpdateChange(new Guid(GetUserGuid()), model);
                ////SandboxContentProviderHost.CurrentProvider.UpdateWeenie(GetUserToken(), model);

                IndexModel indexModel = new IndexModel();
                indexModel.SuccessMessages.Add("Weenie " + model.WeenieClassId.ToString() + " successfully saved.");
                CurrentIndexModel = indexModel;

                ////return SandboxAction();
                return OkResult();
            }
            catch (Exception ex)
            {
                model.ErrorMessages.Add("Error saving weenie in the API");
                model.Exception = ex;
            }

            return ErrorResult("Save Failed");
        }

        [HttpGet]
        ////[Authorize]
        public ActionResult Edit(uint id)
        {
            Weenie model = GetWeenieForUser(id, GetUserGuid());
            ////Weenie model = SandboxContentProviderHost.CurrentProvider.GetWeenie(GetUserToken(), id);
            SortTheThings(model);
            return View(model);
        }

        private void SortTheThings(Weenie model)
        {
            if (model == null)
                return;

            var deprecatedWeenieTypeProperty = model.IntStats.FirstOrDefault(ip => ip.Key == 9007);
            if (deprecatedWeenieTypeProperty != null)
                model.IntStats.Remove(deprecatedWeenieTypeProperty);

            // Setting sort order for edit
            model.IntStats = model.IntStats.OrderBy(ip => ip.Key).ToList();
            model.FloatStats = model.FloatStats.OrderBy(dp => dp.Key).ToList();
        }

        [HttpGet]
        [Authorize]
        public ActionResult Clone(uint id, string userGuid)
        {
            Weenie weenie = GetWeenieForUser(id, userGuid);

            if (weenie == null)
                return new HttpNotFoundResult();

            weenie.IsCloneMode = true;
            return View("New", weenie);

            ////Weenie model = SandboxContentProviderHost.CurrentProvider.GetWeenie(GetUserToken(), id);
            ////model = JsonConvert.DeserializeObject<Weenie>(JsonConvert.SerializeObject(model));
            ////model.WeenieId = 0;
            ////model.IsCloneMode = true;
            ////ImportedWeenie = model;
            ////return View("New", model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Clone(Weenie model)
        {
            ActionResult result = HandlePostback(model);

            if (result != null)
                return result;

            result = ValidateWeenie(model);

            if (result != null)
                return result;

            model.CleanDeletedAndEmptyProperties();
            model.LastModified = DateTime.Now;
            model.ModifiedBy = GetUserName();

            try
            {
                ////SandboxContentProviderHost.CurrentProvider.CreateWeenie(GetUserToken(), model);
                SandboxProvider.UpdateChange(new Guid(GetUserGuid()), model);

                IndexModel indexModel = new IndexModel();
                indexModel.SuccessMessages.Add("Weenie " + model.WeenieClassId.ToString() + " successfully created.");
                CurrentIndexModel = indexModel;

                return SandboxAction();
            }
            catch (Exception ex)
            {
                model.ErrorMessages.Add("Error saving weenie in the API");
                model.Exception = ex;
            }

            return View("New", model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult New()
        {
            ViewBag.UserGuid = null;
            Weenie model = new Weenie();

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult EditImported()
        {
            throw new NotImplementedException("EditImported");

            ////Weenie model = ImportedWeenie;

            ////if (model == null)
            ////    RedirectToAction("Upload");

            ////Weenie existing = SandboxContentProviderHost.CurrentProvider.GetWeenie(GetUserToken(), model.WeenieClassId);
            ////if (existing == null)
            ////    return RedirectToAction("NewImported");

            ////SortTheThings(model);

            ////return View("Edit", model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult NewImported()
        {
            throw new NotImplementedException("NewImported");

            ////Weenie model = ImportedWeenie;

            ////if (model == null)
            ////    RedirectToAction("Upload");

            ////SortTheThings(model);

            ////return View("New", model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult New(Weenie model)
        {
            ActionResult result = HandlePostback(model);

            if (result != null)
                return result;

            result = ValidateWeenie(model);

            if (result != null)
                return result;

            model.CleanDeletedAndEmptyProperties();
            model.LastModified = DateTime.Now;
            model.ModifiedBy = GetUserName();

            try
            {
                SandboxProvider.UpdateChange(new Guid(GetUserGuid()), model);
                ////SandboxContentProviderHost.CurrentProvider.CreateWeenie(GetUserToken(), model);

                IndexModel indexModel = new IndexModel();
                indexModel.SuccessMessages.Add("Weenie " + model.WeenieClassId.ToString() + " successfully created.");
                CurrentIndexModel = indexModel;

                return SandboxAction();
            }
            catch (Exception ex)
            {
                model.ErrorMessages.Add("Error saving weenie in the API");
                model.Exception = ex;
            }

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Edit(Weenie model)
        {
            ActionResult result = HandlePostback(model);

            if (result != null)
            {
                ModelState.Clear();
                return result;
            }

            result = ValidateWeenie(model);

            if (result != null)
            {
                ModelState.Clear();
                return result;
            }

            model.CleanDeletedAndEmptyProperties();
            model.LastModified = DateTime.Now;
            model.ModifiedBy = GetUserName();

            try
            {
                SandboxProvider.UpdateChange(new Guid(GetUserGuid()), model);
                ////SandboxContentProviderHost.CurrentProvider.UpdateWeenie(GetUserToken(), model);

                IndexModel indexModel = new IndexModel();
                indexModel.SuccessMessages.Add("Weenie " + model.WeenieClassId.ToString() + " successfully saved.");
                CurrentIndexModel = indexModel;

                return SandboxAction();
            }
            catch (Exception ex)
            {
                model.ErrorMessages.Add("Error saving weenie in the API");
                model.Exception = ex;
            }

            return View(model);
        }

        private ActionResult ValidateWeenie(Weenie model)
        {
            // ModelState.Clear();
            // TryValidateModel(model);

            bool isValid = true;

            // validatate no duplicate properties
            var dupeInts = model.IntStats.GroupBy(p => p.Key).SelectMany(s => s.Skip(1));
            if (dupeInts.Count() > 0)
            {
                isValid = false;
                dupeInts.ToList().ForEach(dupe => model.ErrorMessages.Add($"Duplicate Int property {dupe.Key} - you may only have one."));
            }

            var dupeInt64s = model.Int64Stats.GroupBy(p => p.Key).SelectMany(s => s.Skip(1));
            if (dupeInt64s.Count() > 0)
            {
                isValid = false;
                dupeInt64s.ToList().ForEach(dupe => model.ErrorMessages.Add($"Duplicate Int64 property {dupe.Key} - you may only have one."));
            }

            var dupeDoubles = model.FloatStats.GroupBy(p => p.Key).SelectMany(p => p.Skip(1));
            if (dupeDoubles.Count() > 0)
            {
                isValid = false;
                dupeDoubles.ToList().ForEach(dupe => model.ErrorMessages.Add($"Duplicate Double property {dupe.Key} - you may only have one."));
            }

            var dupeStrings = model.StringStats.GroupBy(p => p.Key).SelectMany(p => p.Skip(1));
            if (dupeStrings.Count() > 0)
            {
                isValid = false;
                dupeStrings.ToList().ForEach(dupe => model.ErrorMessages.Add($"Duplicate String property {dupe.Key} - you may only have one."));
            }

            var dupeIids = model.IidStats.GroupBy(p => p.Key).SelectMany(p => p.Skip(1));
            if (dupeIids.Count() > 0)
            {
                isValid = false;
                dupeIids.ToList().ForEach(dupe => model.ErrorMessages.Add($"Duplicate Instance ID property {dupe.Key} - you may only have one."));
            }

            var dupeDids = model.FloatStats.GroupBy(p => p.Key).SelectMany(p => p.Skip(1));
            if (dupeDids.Count() > 0)
            {
                isValid = false;
                dupeDids.ToList().ForEach(dupe => model.ErrorMessages.Add($"Duplicate Data ID property {dupe.Key} - you may only have one."));
            }

            var dupeBools = model.BoolStats.GroupBy(p => p.Key).SelectMany(p => p.Skip(1));
            if (dupeBools.Count() > 0)
            {
                isValid = false;
                dupeBools.ToList().ForEach(dupe => model.ErrorMessages.Add($"Duplicate Bool property {dupe.Key} - you may only have one."));
            }

            // var emotes = model.EmoteTable
            //     .SelectMany(et => et.Emotes)
            //     .Where(e => Models.Shared.Emote.IsPropertyVisible("Message", e.e))
            //     .Where(e => string.IsNullOrEmpty(e.Message));
            // if (emotes.Count() > 0)
            // {
            //     isValid = false;
            //     model.ErrorMessages.Add("Tell emotes without required message");
            // }

            if (model.ItemType == null)
            {
                isValid = false;
                model.ErrorMessages.Add("Item Type is required.");
            }

            if (model.WeenieTypeId == 0)
            {
                isValid = false;
                model.ErrorMessages.Add("Weenie Type is required.");
            }

            if (!ModelState.IsValid || !isValid)
            {
                foreach (KeyValuePair<string, ModelState> pair in ModelState)
                {
                    ModelState state = pair.Value;

                    foreach (ModelError error in state.Errors)
                    {
                        model.ErrorMessages.Add(error.ErrorMessage);
                        isValid = false;
                    }
                }
            }

            SortTheThings(model);

            if (!isValid)
                return View(model);

            return null;
        }

        private ActionResult HandlePostback(Weenie model)
        {
            model.CleanDeletedAndEmptyProperties();

            switch (model.MvcAction)
            {
                case WeenieCommands.AddIntProperty:
                    if (model.NewIntPropertyId == null)
                        model.ErrorMessages.Add("You must select a Property to add.");
                    else
                        model.IntStats.Add(new IntStat() { Key = (int)model.NewIntPropertyId.Value });

                    model.NewIntPropertyId = null;
                    break;

                case WeenieCommands.AddStringProperty:
                    if (model.NewStringPropertyId == null)
                        model.ErrorMessages.Add("You must select a Property to add.");
                    else
                        model.StringStats.Add(new StringStat() { Key = (int)model.NewStringPropertyId.Value });

                    model.NewStringPropertyId = null;
                    break;

                case WeenieCommands.AddInt64Property:
                    if (model.NewInt64PropertyId == null)
                        model.ErrorMessages.Add("You must select a Property to add.");
                    else
                        model.Int64Stats.Add(new Int64Stat() { Key = (int)model.NewInt64PropertyId.Value });

                    model.NewInt64PropertyId = null;
                    break;

                case WeenieCommands.AddDoubleProperty:
                    if (model.NewDoublePropertyId == null)
                        model.ErrorMessages.Add("You must select a Property to add.");
                    else
                        model.FloatStats.Add(new FloatStat() { Key = (int)model.NewDoublePropertyId.Value });

                    model.NewDoublePropertyId = null;
                    break;

                case WeenieCommands.AddDidProperty:
                    if (model.NewDidPropertyId == null)
                        model.ErrorMessages.Add("You must select a Property to add.");
                    else
                        model.DidStats.Add(new DidStat() { Key = (int)model.NewDidPropertyId.Value });

                    model.NewDidPropertyId = null;
                    break;

                case WeenieCommands.AddIidProperty:
                    if (model.NewIidPropertyId == null)
                        model.ErrorMessages.Add("You must select a Property to add.");
                    else
                        model.IidStats.Add(new IidStat() { Key = (int)model.NewIidPropertyId.Value });

                    model.NewIidPropertyId = null;
                    break;

                case WeenieCommands.AddBoolProperty:
                    if (model.NewBoolPropertyId == null)
                        model.ErrorMessages.Add("You must select a Property to add.");
                    else
                        model.BoolStats.Add(new BoolStat() { Key = (int)model.NewBoolPropertyId.Value });

                    model.NewBoolPropertyId = null;
                    break;

                case WeenieCommands.AddSpell:
                    if (model.NewSpellId == null)
                        model.ErrorMessages.Add("You must select a Spell to add.");
                    else
                    {
                        if (model.Spells == null)
                            model.Spells = new List<SpellbookEntry>();
                        model.Spells.Add(new SpellbookEntry() { SpellId = (int)model.NewSpellId.Value });
                    }

                    model.NewSpellId = null;
                    break;

                case WeenieCommands.AddPosition:
                    if (model.NewPositionType == null)
                        model.ErrorMessages.Add("You must select a Position Type to add.");
                    else
                    {
                        if (model.Positions == null)
                            model.Positions = new List<PositionListing>();
                        model.Positions.Add(new PositionListing() { Key = (int)model.NewPositionType.Value });
                    }

                    break;

                case WeenieCommands.AddBookPage:
                    if (model.Book == null)
                        model.Book = new Book();
                    model.Book.Pages.Add(new Page());
                    break;

                case WeenieCommands.AddEmoteSet:
                    if (model.EmoteTable == null)
                        model.EmoteTable = new List<EmoteCategoryListing>();

                    if (model.EmoteTable.All(ecl => ecl.EmoteCategoryId != (int)model.NewEmoteCategory))
                        model.EmoteTable.Add(new EmoteCategoryListing() { EmoteCategoryId = (int)model.NewEmoteCategory });

                    model.EmoteTable.First(ecl => ecl.EmoteCategoryId == (int)model.NewEmoteCategory)
                        .Emotes.Add(new Emote() { Category = (uint)model.NewEmoteCategory });
                    model.NewEmoteCategory = EmoteCategory.Invalid;
                    break;

                case WeenieCommands.AddGeneratorTable:
                    if (model.GeneratorTable == null)
                        model.GeneratorTable = new List<GeneratorTable>();
                    model.GeneratorTable.Add(new GeneratorTable());
                    break;

                case WeenieCommands.AddEmote:
                    EmoteCategoryListing emoteTable = model.EmoteTable
                        .Where(et => et.EmoteCategoryId == (int)model.NewEmoteCategory).FirstOrDefault();

                    if (emoteTable == null || model.EmoteSetGuid == null)
                    {
                        model.ErrorMessages.Add("Invalid Emote Set");
                    }
                    else
                    {
                        Emote emote = emoteTable.Emotes[model.EmoteSetGuid.Value];
                        if (emote.Actions == null)
                            emote.Actions = new List<EmoteAction>();

                        int order = emote.Actions.Max(e => e.SortOrder) + 1 ?? 1;
                        emote.Actions.Add(new EmoteAction()
                        {
                            SortOrder = order,
                            EmoteActionType = (uint)emote.NewEmoteType.Value,
                        });
                        emote.NewEmoteType = EmoteType.Invalid;
                    }

                    model.NewEmoteCategory = EmoteCategory.Invalid;

                    break;

                case WeenieCommands.AddSkill:
                    if (model.NewSkillId == null)
                        model.ErrorMessages.Add("You must select a skill to add.");
                    else
                    {
                        if (model.Skills == null)
                            model.Skills = new List<SkillListing>();
                        model.Skills.Add(new SkillListing() { SkillId = (int)model.NewSkillId.Value });
                    }

                    break;

                case WeenieCommands.AddCreateItem:
                    if (model.CreateList == null)
                        model.CreateList = new List<CreateItem>();
                    model.CreateList.Add(new CreateItem());
                    break;

                case WeenieCommands.AddBodyParts:
                    model.Body = model.Body ?? new Body();
                    model.Body.BodyParts.Add(new BodyPartListing()
                    {
                        Key = 0
                    });

                    break;

                case WeenieCommands.AddBodyPart:
                    int key = model.Body.BodyParts.Max(bpl => bpl.Key) + 1;
                    model.Body.BodyParts.Add(new BodyPartListing()
                    {
                        Key = key,
                        BodyPartType = model.NewBodyPartType ?? BodyPartType.Head
                    });
                    break;

                case null:
                    return null;
            }

            SortTheThings(model);
            model.MvcAction = null;
            model.NewPositionType = null;
            return View(model);
        }

        [HttpGet]
        public ActionResult WeenieFinder(uint id)
        {
            Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
            Response.Cache.SetMaxAge(new TimeSpan(1, 0, 0));

            List<WeenieSearchResult> results = null;
            Expression<Func<Weenie, bool>> criteria = (w) => w.WeenieClassId == id;

            if (User.Identity.IsAuthenticated)
            {
                Guid uid = new Guid(GetUserGuid());
                results = SandboxProvider.Search(uid, criteria).Select(w => w.ToSearchResult()).ToList();
            }
            else
            {
                results = Provider.Search(criteria).Select(w => w.ToSearchResult()).ToList();
            }

            ////List<WeenieSearchResult> results = ContentProviderHost.CurrentProvider.WeenieSearch(GetUserToken(), model);
            return JsonGet(results);

            ////return View(new SearchWeeniesCriteria());
        }

        [HttpPost]
        public JsonResult WeenieFinder(SearchWeeniesCriteria model)
        {
            List<WeenieSearchResult> results = null;
            var criteria = ProcessSearchCriteria(model);

            if (User.Identity.IsAuthenticated)
            {
                Guid uid = new Guid(GetUserGuid());
                results = SandboxProvider.Search(uid, criteria).Select(w => w.ToSearchResult()).ToList();
            }
            else
            {
                results = Provider.Search(criteria).Select(w => w.ToSearchResult()).ToList();
            }

            ////List<WeenieSearchResult> results = ContentProviderHost.CurrentProvider.WeenieSearch(GetUserToken(), model);
            return Json(results);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadEx()
        {
            string fileNameCopy = "n/a";

            try
            {
                foreach (string fileName in Request.Files)
                {
                    fileNameCopy = fileName;
                    HttpPostedFileBase file = Request.Files[fileName];
                    uint weenieId = 0;

                    using (MemoryStream memStream = new MemoryStream())
                    {
                        file.InputStream.CopyTo(memStream);
                        byte[] data = memStream.ToArray();

                        string serialized = Encoding.UTF8.GetString(data);
                        ImportedWeenie = JsonConvert.DeserializeObject<Weenie>(serialized);
                        weenieId = ImportedWeenie.WeenieClassId;
                    }

                    return Json(new { id = weenieId });
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error parsing uploaded weenie {fileNameCopy}.", ex);
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadEx2(bool done)
        {
            string fileNameCopy = "n/a";

            try
            {
                Guid uid = new Guid(GetUserGuid());
                foreach (string fileName in Request.Files)
                {
                    fileNameCopy = fileName;
                    HttpPostedFileBase file = Request.Files[fileName];
                    ////uint weenieId = 0;

                    using (MemoryStream memStream = new MemoryStream())
                    {
                        file.InputStream.CopyTo(memStream);
                        byte[] data = memStream.ToArray();

                        string serialized = Encoding.UTF8.GetString(data);
                        Weenie weenie = JsonConvert.DeserializeObject<Weenie>(serialized);
                        ////weenieId = weenie.WeenieClassId;

                        ////string token = GetUserToken();

                        weenie.LastModified = DateTime.Now;
                        weenie.ModifiedBy = GetUserName();
                        if (done)
                            weenie.IsDone = done;

                        // save it to the sandbox
                        SandboxProvider.UpdateChange(uid, weenie);
                        ////SandboxContentProviderHost.CurrentProvider.CreateWeenie(token, weenie);
                        ////ContentProviderHost.CurrentWeenieProvider.Save(weenie);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error parsing uploaded weenie {fileNameCopy}.", ex);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult DownloadOriginal(uint id)
        {
            try
            {
                Weenie model = Provider.Get(id);
                ////Weenie model = SandboxContentProviderHost.CurrentProvider.GetWeenieFromSource(GetUserToken(), id);
                return DownloadJson(model, id, model.Name);
            }
            catch (Exception ex)
            {
                log.Error($"Error exporting weenie {id}", ex);

                IndexModel model = new IndexModel();
                model.ErrorMessages.Add($"Error exporting weenie {id}");
                model.Exception = ex;
                CurrentIndexModel = model;

                return RedirectToAction("Index");
            }
        }

        ////[HttpGet]
        ////[Authorize]
        ////public ActionResult Sandbox()
        ////{
        ////    SandboxModel model = new SandboxModel();
        ////    BaseModel.CopyBaseData(CurrentBaseModel, model);

        ////    model.Entries = SandboxContentProviderHost.CurrentProvider.GetMyWeenieChanges(GetUserToken());

        ////    return View(model);
        ////}

        ////[HttpGet]
        ////[Authorize(Roles = "Developer")]
        ////public ActionResult Submissions()
        ////{
        ////    SandboxModel model = new SandboxModel();
        ////    BaseModel current = CurrentBaseModel;
        ////    BaseModel.CopyBaseData(current, model);
        ////    CurrentBaseModel = current;

        ////    if (User.IsInRole("Developer"))
        ////    {
        ////        List<WeenieChange> temp = SandboxContentProviderHost.CurrentProvider.GetAllWeenieChanges();
        ////        model.Entries = temp.Where(x => x.Submitted).ToList();
        ////    }
        ////    else
        ////        model.Entries = SandboxContentProviderHost.CurrentProvider.GetMyWeenieChanges(GetUserToken());

        ////    return View(model);
        ////}

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(uint id)
        {
            Weenie model = Provider.Get(id);
            ////Weenie model = SandboxContentProviderHost.CurrentProvider.GetWeenie(GetUserToken(), id);
            if (model == null)
                return RedirectToAction("Index");

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Weenie model)
        {
            try
            {
                if (model.MvcAction == WeenieCommands.Delete)
                {
                    IndexModel indexModel = new IndexModel();
                    Provider.Delete(model.WeenieClassId);
                    ////SandboxContentProviderHost.CurrentProvider.DeleteWeenie(GetUserToken(), model.WeenieClassId);
                    indexModel.SuccessMessages.Add("Weenie " + model.WeenieClassId.ToString() + " successfully deleted.");
                    CurrentIndexModel = indexModel;
                }
            }
            catch (Exception ex)
            {
                model.ErrorMessages.Add("Error saving weenie in the API");
                model.Exception = ex;
                return View(model);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public ActionResult DownloadSandbox(uint id, string userGuid)
        {
            WeenieChange wc = null;

            if (string.IsNullOrWhiteSpace(userGuid))
            {
                // no userGuid specified, assume your own sandbox
                ////wc = SandboxContentProviderHost.CurrentProvider.GetMySandboxedChange(GetUserToken(), id);
                wc = SandboxProvider.GetChange(new Guid(GetUserGuid()), id) as WeenieChange;
            }
            else if (User.IsInRole("Developer") || GetUserGuid() == userGuid)
            {
                // validate dev/admin or matches your id
                ////wc = SandboxContentProviderHost.CurrentProvider.GetSandboxedChange(Guid.Parse(userGuid), id);
                wc = SandboxProvider.GetChange(new Guid(userGuid), id) as WeenieChange;
            }

            if (wc == null)
                return new HttpNotFoundResult();

            return DownloadJson(wc.Entry, id, wc.Entry.Name);
        }

        [HttpPost]
        public ActionResult DownloadCustom(uint[] ids, string[] props)
        {
            if (ids == null || ids.Length == 0)
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            List<Weenie> results = new List<Weenie>(ids.Length);

            foreach (uint id in ids)
                results.Add(Provider.Get(id));
            ////results.Add(ContentProviderHost.CurrentProvider.GetWeenie(GetUserToken(), id));

            // https://blog.rsuter.com/advanced-newtonsoft-json-dynamically-rename-or-ignore-properties-without-changing-the-serialized-class/

            if (props != null && props.Length > 0)
            {
                JsonNetIncludePropertyResolver resolver = new JsonNetIncludePropertyResolver();
                resolver.Include(typeof(Weenie), props);
                return JsonGet(results, resolver);
            }

            return JsonGet(results, true);
        }

        private Expression<Func<Weenie, bool>> ProcessSearchCriteria(SearchWeeniesCriteria criteria)
        {
            // TODO: Needs a lot of work
            // build the criteria expression
            ParameterExpression ew = Expression.Parameter(typeof(Weenie));
            LambdaExpression exp = null;

            if (criteria.WeenieClassId != null)
            {
                Expression<Func<Weenie, bool>> e = w => w.WeenieClassId == criteria.WeenieClassId.Value;
                exp = e;
            }

            if (criteria.WeenieType != null)
            {
                // until the query builder is smarter, cast before
                int wt = (int)criteria.WeenieType.Value;
                Expression<Func<Weenie, bool>> e = w => w.WeenieTypeId == wt;

                if (exp != null)
                    exp = Expression.Lambda<Func<Weenie, bool>>(Expression.AndAlso(exp.Body, e.Body), ew);
                else
                    exp = e;
            }

            if (!string.IsNullOrEmpty(criteria.PartialName))
            {
                Expression<Func<Weenie, bool>> e = w => w.StringStats.Any(ss => ss.Key == 1 && ss.Value == criteria.PartialName);
                ////Expression<Func<Weenie, bool>> e = w => w.StringStats[0].Value == criteria.PartialName;

                if (exp != null)
                    exp = Expression.Lambda<Func<Weenie, bool>>(Expression.AndAlso(exp.Body, e.Body), ew);
                else
                    exp = e;
            }

            if (criteria.ItemType != null)
            {
                int it = (int)criteria.ItemType.Value;
                Expression<Func<Weenie, bool>> e = w => w.IntStats.Any(i => i.Key == 1 && i.Value == it);
                if (exp != null)
                    exp = Expression.Lambda<Func<Weenie, bool>>(Expression.AndAlso(exp.Body, e.Body), ew);
                else
                    exp = e;
            }

            foreach (var prop in criteria.PropertyCriteria)
            {
                Expression<Func<Weenie, bool>> e = null;

                switch (prop.PropertyType)
                {
                    case PropertyType.PropertyBool:
                        e = w => w.BoolStats.Any(p => p.Key == prop.PropertyId && p.Value == (bool.Parse(prop.PropertyValue) ? 1 : 0));
                        break;
                    case PropertyType.PropertyDataId:
                        e = w => w.DidStats.Any(p => p.Key == prop.PropertyId && p.Value == uint.Parse(prop.PropertyValue));
                        break;
                    case PropertyType.PropertyDouble:
                        e = w => w.FloatStats.Any(p => p.Key == prop.PropertyId && p.Value == float.Parse(prop.PropertyValue));
                        break;
                    case PropertyType.PropertyInstanceId:
                        e = w => w.IidStats.Any(p => p.Key == prop.PropertyId && p.Value == uint.Parse(prop.PropertyValue));
                        break;
                    case PropertyType.PropertyInt:
                        e = w => w.IntStats.Any(p => p.Key == prop.PropertyId && p.Value == int.Parse(prop.PropertyValue));
                        break;
                    case PropertyType.PropertyInt64:
                        e = w => w.Int64Stats.Any(p => p.Key == prop.PropertyId && p.Value == long.Parse(prop.PropertyValue));
                        break;
                    case PropertyType.PropertyString:
                        e = w => w.StringStats.Any(p => p.Key == prop.PropertyId && p.Value == prop.PropertyValue);
                        break;

                    case PropertyType.PropertyAttribute:
                    case PropertyType.PropertyAttribute2nd:
                    case PropertyType.PropertyBook:
                    case PropertyType.PropertyPosition:
                    default:
                        continue;
                }

                if (e == null)
                    continue;

                if (exp != null)
                    exp = Expression.Lambda<Func<Weenie, bool>>(Expression.AndAlso(exp.Body, e.Body), ew);
                else
                    exp = e;
            }

            Expression<Func<Weenie, bool>> lambda = null;
            if (exp != null)
                lambda = Expression.Lambda<Func<Weenie, bool>>(exp.Body, ew);

            return lambda;
        }

        ////[HttpGet]
        ////[Authorize]
        ////public ActionResult DeleteSandboxWeenie(uint id)
        ////{
        ////    List<WeenieChange> data = SandboxContentProviderHost.CurrentProvider.GetMyWeenieChanges(GetUserToken());

        ////    WeenieChange theOne = data.FirstOrDefault(wc => wc.Entry.WeenieClassId == id);
        ////    if (theOne != null)
        ////        SandboxContentProviderHost.CurrentProvider.DeleteWeenieChange(theOne.UserGuid, theOne);

        ////    return SandboxAction();
        ////}

        ////[HttpPost]
        ////[Authorize]
        ////public ActionResult AddDiscussionComment(string userGuid, int itemId, string discussionComment, string source)
        ////{
        ////    string currentUser = GetUserGuid();

        ////    if (!(User.IsInRole("Developer") || userGuid == currentUser))
        ////    {
        ////        // only the submitter and developers can comment
        ////        return RedirectToAction(source);
        ////    }

        ////    List<WeenieChange> temp = SandboxContentProviderHost.CurrentProvider.GetAllWeenieChanges();
        ////    WeenieChange change = temp.FirstOrDefault(x => x.UserGuid == userGuid && x.Entry.WeenieClassId == itemId);

        ////    if (change == null)
        ////        return RedirectToAction(source);

        ////    change.Discussion.Add(new ChangeDiscussionEntry()
        ////    {
        ////        Comment = discussionComment,
        ////        Created = DateTime.Now,
        ////        UserName = GetUserName(),
        ////        UserGuid = Guid.Parse(GetUserGuid())
        ////    });

        ////    SandboxContentProviderHost.CurrentProvider.UpdateWeenieChange(userGuid, change);

        ////    return RedirectToAction(source);
        ////}
    }
}
