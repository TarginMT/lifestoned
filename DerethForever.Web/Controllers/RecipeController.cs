/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

using log4net;
using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle.Recipes;
using Lifestoned.DataModel.Shared;
using Lifestoned.Providers;

namespace DerethForever.Web.Controllers
{
    public class RecipeController : BaseController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IRecipeProvider Provider => ContentProviderHost.GetProvider<IRecipeProvider>(RecipeChange.TypeName);

        private IRecipeSandboxProvider SandboxProvider => SandboxContentProviderHost.GetProvider<IRecipeSandboxProvider>(RecipeChange.TypeName);

        [HttpGet]
        public ActionResult Index()
        {
            return View(Provider.Get());
        }

        [HttpGet]
        public ActionResult Get(uint id, string userGuid)
        {
            Guid userId = Guid.Empty;

            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrWhiteSpace(userGuid) && (userGuid == GetUserGuid() || User.IsInRole("Developer")))
                {
                    userId = new Guid(userGuid);
                }
                else
                {
                    userId = new Guid(GetUserGuid());
                }
            }

            ChangeEntry<Recipe> change = SandboxProvider.GetChange(userId, id);
            if (change != null)
                return JsonGet(change.Entry, true);

            Recipe model = Provider.Get(id);
            return JsonGet(model, true);
        }

        [HttpGet]
        public ActionResult Search(string name, uint? tool, uint? target, uint? result)
        {
            IQueryable<Recipe> model = null;
            if (User.Identity.IsAuthenticated)
            {
                model = SandboxProvider.Search(
                    new Guid(GetUserGuid()),
                    (r) => (!string.IsNullOrWhiteSpace(name) ? r.Description == name : true)
                        && (tool != null ? r.Precursors.Any(p => p.Tool == tool) : true)
                        && (target != null ? r.Precursors.Any(p => p.Target == target) : true)
                        && (result != null ? r.RecipeData.SuccessWcid == result : true));
            }
            else
            {
                model = Provider.Search((r) => (!string.IsNullOrWhiteSpace(name) ? r.Description == name : true)
                    && (tool != null ? r.Precursors.Any(p => p.Tool == tool) : true)
                    && (target != null ? r.Precursors.Any(p => p.Target == target) : true)
                    && (result != null ? r.RecipeData.SuccessWcid == result : true));
            }

            return JsonGet(model, true);
        }

        [HttpPut]
        [Authorize]
        public ActionResult Put([ModelBinder(typeof(JsonNetModelBinder))]Recipe model)
        {
            model.LastModified = DateTime.Now;
            model.ModifiedBy = GetUserName();

            Guid userId = new Guid(GetUserGuid());

            try
            {
                SandboxProvider.UpdateChange(userId, model);
                ////ContentProviderHost.CurrentRecipeProvider.Save(model);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                log.Error($"Error Saving Recipe {model.Key} - {model.Description}", ex);
            }

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError, "Save Failed");
        }

        [HttpGet]
        public ActionResult Detail(uint id, string userGuid)
        {
            ViewBag.UserGuid = userGuid;
            return View(id);
        }

        [HttpGet]
        public ActionResult Edit(uint? id)
        {
            return View(id);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(uint id)
        {
            return View(Provider.Get(id));
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Recipe model)
        {
            Provider.Delete(model.Key);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadItem()
        {
            string fileNameCopy = "n/a";

            try
            {
                foreach (string fileName in Request.Files)
                {
                    fileNameCopy = fileName;
                    HttpPostedFileBase file = Request.Files[fileName];
                    uint id = 0;

                    using (MemoryStream memStream = new MemoryStream())
                    {
                        file.InputStream.CopyTo(memStream);
                        byte[] data = memStream.ToArray();

                        string serialized = Encoding.UTF8.GetString(data);
                        Recipe item = JsonConvert.DeserializeObject<Recipe>(serialized);
                        id = item.Key;

                        string token = GetUserToken();

                        item.LastModified = DateTime.Now;
                        item.ModifiedBy = GetUserName();

                        Guid userId = new Guid(GetUserGuid());

                        // save it
                        ////ContentProviderHost.CurrentRecipeProvider.Save(item);
                        SandboxProvider.UpdateChange(userId, item);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error parsing uploaded file {fileNameCopy}.", ex);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult DownloadOriginal(uint id)
        {
            try
            {
                Recipe model = Provider.Get(id);
                return DownloadJson(model, model.Key, model.Description);
            }
            catch (Exception ex)
            {
                log.Error($"Error exporting recipe {id}", ex);
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult DownloadSandbox(uint id, string userGuid)
        {
            Guid userId = Guid.Empty;

            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrWhiteSpace(userGuid) && (userGuid == GetUserGuid() || User.IsInRole("Developer")))
                {
                    userId = new Guid(userGuid);
                }
                else
                {
                    userId = new Guid(GetUserGuid());
                }
            }

            try
            {
                ChangeEntry<Recipe> change = SandboxProvider.GetChange(userId, id);
                if (change == null)
                {
                    return HttpNotFound();
                }

                return DownloadJson(change.Entry, change.EntryId, change.Entry.Description);
            }
            catch (Exception ex)
            {
                log.Error($"Error exporting recipe {id}", ex);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }
    }
}
