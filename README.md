# Lifestoned

## Requirements

* Git
* Microsoft Visual Studio
* Microsoft PowerShell
* MySQL/MariaDB
* Working Asheron's Call `client_portal.dat`
* [An Existing Lifestoned World Release](https://lifestoned.org/WorldRelease)

## Setup

Pick a location where you want to install lifestoned.  In this example I will use the base directory `C:\src` and install lifestoned to `C:\src\lifestoned` and store the lifestoned data in `C:\src\lifestoned\data`.

1. Open a powershell terminal
2. Run the following
    ```powershell
    cd C:\src
    git clone https://gitlab.com/Scribble/lifestoned.git
    cd lifestoned
    .\pre-setup.ps1
    ```
3. In the first dialog navigate to where we cloned the code to e.g. `c:\src\lifestoned`
4. In the second dialog navigate to the location of your Asheron's Call `client_portal.dat` location
5. Create a username and database owned by that user in your mysql server in this example I will be using the following snippet which is not meant for production
    ```sql
    CREATE DATABASE lifestoned CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
    CREATE USER 'lifestoned'@'localhost' IDENTIFIED BY 'lifestoned';
    GRANT ALL PRIVILEGES ON *.* TO 'lifestoned'@'localhost';
    FLUSH PRIVILEGES;
    ```
6. Import the required authentication tables by running the sql script under `sqldumps/0_auth_base.sql`
7. Copy `DerethForever.Web\Web.example.config` to `DerethForever.Web\Web.config` then open in your favorite text editor
8. Update the following values
    1. `<add key="SandboxCacheDir" value="C:\src\lifestoned\data\Sandboxes\" />`
    2. `<add key="WorldReleaseDir" value="C:\src\lifestoned\data\WorldReleases\" />`
    3. `<add key="FinalDir" value="C:\src\lifestoned\data\Final\" />`
    4. `<add key="LocalAuthProviderDir" value="C:\src\lifestoned\data\LocalAuthProvider\" />`
    5. `<add key="PortalDat" value="c:\src\lifestoned\data\client_portal.dat"/>`
    6. `<add key="AuthProvider" value="Lifestoned.Providers.DatabaseAuthProvider"/>`
    7. `<add name="AuthDbConnection" connectionString="Server=127.0.0.1;User Id=lifestoned;Password=lifestoned;Database=lifestoned;IgnorePrepare=false;SslMode=None;" providerName="MySql.Data.MySqlClient" />`
9. Open Lifestoned.sln in Visual Studio
10. Right click on `DerethForever.Web` in the Visual Studio explorer and make sure it is `Set as Startup Project`
11. Click the Green Play icon to build and launch IIS Express, your default browser should eventually load showing main page
12. Click `Register`
13. Enter information for your admin user, Click `Register` at the bottom
14. After you see `Registration success.`
15. Run the following mysql command to update your account to an Admin, note in this case I made the username of my admin account `admin`
    ```sql
    USE lifestoned;
    UPDATE subscription AS s
    INNER JOIN account AS a ON a.accountGuid = s.accountGuid
    SET s.accessLevel = 5
    WHERE accountName = "admin";
    ```
16. Back in your browser refresh the page you should now see some additional menu items
17. You should now have a working copy of Lifestoned

__Good Luck, Have Fun__